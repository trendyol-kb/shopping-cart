package com.trendyolkb.shoppingcart.service.cart;

import com.trendyolkb.shoppingcart.domain.Category;
import com.trendyolkb.shoppingcart.domain.Product;
import com.trendyolkb.shoppingcart.domain.discount.Campaign;
import com.trendyolkb.shoppingcart.domain.discount.Coupon;
import com.trendyolkb.shoppingcart.domain.discount.DiscountType;
import com.trendyolkb.shoppingcart.service.delivery.DeliveryCostCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ShoppingCartTest {

	ShoppingCart shoppingCart;
	// Define categories
	Category categoryFood = new Category("food");
	Category categoryDigital = new Category("digital");

	// Define products
	Product apple = new Product("Apple", 100.0, categoryFood);
	Product almond = new Product("Almond", 150.0, categoryFood);

	Product tv = new Product("TV", 2000.0, categoryDigital);

	// Define campaigns
	Campaign campaign1 = new Campaign(categoryFood, 20.0, 3, DiscountType.Rate);
	Campaign campaign2 = new Campaign(categoryFood, 50.0, 5, DiscountType.Rate);

	Campaign campaign3 = new Campaign(categoryDigital, 200, 2, DiscountType.Amount);

	// Define coupons
	Coupon coupon = new Coupon(1500, 200, DiscountType.Amount);

	@BeforeEach
	public void setup() {
		shoppingCart = new ShoppingCart();
	}

	@Test
	public void shouldCreateEmptyShoppingCart() {
		assertNotNull(shoppingCart);
		assertEquals(0.0, shoppingCart.getCampaignDiscount());
		assertEquals(0.0, shoppingCart.getCouponDiscount());
		assertEquals(0.0, shoppingCart.getTotalAmountAfterDiscounts());
		assertEquals(0.0, shoppingCart.getDeliveryCost());
		assertEquals(0, shoppingCart.getProductCount());
	}

	@Test
	public void shouldCountProductsRegardlessOfQuantity() {
		shoppingCart.addItem(apple, 1);
		assertEquals(1, shoppingCart.getProductCount());

		shoppingCart.addItem(almond, 2);
		assertEquals(2, shoppingCart.getProductCount());
	}

	@Test
	public void shouldNotApplyCampaignDiscountIfMinimumQuantityIsNotSatisfied() {
		int quantity = 1;
		shoppingCart.addItem(apple, quantity);
		shoppingCart.applyDiscounts(campaign1);

		assertEquals(0.0, shoppingCart.getCampaignDiscount());
	}

	@Test
	public void shouldApplyRateDiscountIfMinimumQuantityIsSatisfied() {
		int quantity = 3;
		shoppingCart.addItem(apple, quantity);
		shoppingCart.applyDiscounts(campaign1);

		double discount = apple.getPrice() * quantity * campaign1.getDiscount() / 100;
		assertEquals(discount, shoppingCart.getCampaignDiscount());
	}

	@Test
	public void shouldApplyAmountDiscountCorrectly() {
		int quantity = 2;
		shoppingCart.addItem(tv, quantity);
		shoppingCart.applyDiscounts(campaign3);

		double discount = campaign3.getDiscount();
		assertEquals(discount, shoppingCart.getCampaignDiscount());
	}

	@Test
	public void shouldApplyDiscountOnlyForTheCategoryOfTheCampaign() {
		int quantity = 2;
		shoppingCart.addItem(tv, quantity);
		shoppingCart.addItem(apple, quantity);
		shoppingCart.addItem(almond, quantity);

		shoppingCart.applyDiscounts(campaign3);

		double discount = campaign3.getDiscount();
		assertEquals(discount, shoppingCart.getCampaignDiscount());
	}


	@Test
	public void shouldApplyTheCampaignWithMaximumDiscountInMultipleCampaigns() {
		int apples = 3;
		int almonds = 5;
		shoppingCart.addItem(apple, apples);
		shoppingCart.addItem(almond, almonds);
		shoppingCart.applyDiscounts(campaign1, campaign2);

		double discount = (apple.getPrice() * apples + almond.getPrice() * almonds) * campaign2.getDiscount() / 100;

		assertEquals(discount, shoppingCart.getCampaignDiscount());
	}

	@Test
	public void shouldApplyMultipleDiscountsForMultipleCategories() {
		int quantity = 3;
		shoppingCart.addItem(tv, quantity);
		shoppingCart.addItem(apple, quantity);

		shoppingCart.applyDiscounts(campaign1, campaign3);

		double discount = (apple.getPrice() * quantity * campaign1.getDiscount() / 100) + campaign3.getDiscount();
		assertEquals(discount, shoppingCart.getCampaignDiscount());
	}

	@Test
	public void shouldApplyCouponIfApplicable() {
		int quantity = 1;
		shoppingCart.addItem(tv, quantity);

		shoppingCart.applyCoupon(coupon);

		double discount = coupon.getDiscount();
		assertEquals(discount, shoppingCart.getCouponDiscount());
	}

	@Test
	public void shouldNotApplyCouponIfNotApplicable() {
		int quantity = 1;
		shoppingCart.addItem(apple, quantity);

		shoppingCart.applyCoupon(coupon);

		assertEquals(0.0, shoppingCart.getCouponDiscount());
	}

	@Test
	public void shouldCalculateDeliveryCostCorrectly() {
		int tvs = 2;
		int apples = 10;
		int almonds = 7;
		shoppingCart.addItem(tv, tvs);
		shoppingCart.addItem(apple, apples);
		shoppingCart.addItem(almond, almonds);

		double cost = DeliveryCostCalculator.DEFAULT_COST_PER_DELIVERY * 2 +
				DeliveryCostCalculator.DEFAULT_COST_PER_PRODUCT * 3 +
				DeliveryCostCalculator.DEFAULT_FIXED_COST;

		assertEquals(cost, shoppingCart.getDeliveryCost());
	}

	@Test
	public void shouldApplyMultipleDiscountsAndCoupon() {
		int quantity = 3;
		shoppingCart.addItem(tv, quantity);
		shoppingCart.addItem(apple, quantity);

		shoppingCart.applyDiscounts(campaign1, campaign3);
		shoppingCart.applyCoupon(coupon);

		double campaignDiscount = (apple.getPrice() * quantity * campaign1.getDiscount() / 100) + campaign3.getDiscount();
		double couponDiscount = coupon.getDiscount();

		assertEquals(campaignDiscount, shoppingCart.getCampaignDiscount());
		assertEquals(couponDiscount, shoppingCart.getCouponDiscount());

		double cost = tv.getPrice() * quantity + apple.getPrice() * quantity - (campaignDiscount + couponDiscount);
		assertEquals(cost, shoppingCart.getTotalAmountAfterDiscounts());

		shoppingCart.print();
	}
}