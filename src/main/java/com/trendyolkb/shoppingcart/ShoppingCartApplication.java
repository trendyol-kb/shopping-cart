package com.trendyolkb.shoppingcart;

import com.trendyolkb.shoppingcart.domain.Category;
import com.trendyolkb.shoppingcart.domain.Product;
import com.trendyolkb.shoppingcart.domain.discount.Campaign;
import com.trendyolkb.shoppingcart.domain.discount.Coupon;
import com.trendyolkb.shoppingcart.domain.discount.DiscountType;
import com.trendyolkb.shoppingcart.service.cart.ShoppingCart;

public class ShoppingCartApplication {

	public static void main(String[] args) {

		// Define categories
		Category food = new Category("food");

		// Define products
		Product apple = new Product("Apple", 100.0, food);
		Product almond = new Product("Almond", 150.0, food);

		// Define campaigns
		Campaign campaign1 = new Campaign(food, 20.0, 3, DiscountType.Rate);
		Campaign campaign2 = new Campaign(food, 50.0, 5, DiscountType.Rate);
		Campaign campaign3 = new Campaign(food, 5.0, 5, DiscountType.Amount);

		// Define coupons
		Coupon coupon = new Coupon(100, 10, DiscountType.Rate);

		// Define shopping cart
		ShoppingCart cart = new ShoppingCart();

		// Add items
		cart.addItem(apple, 3);
		cart.addItem(almond, 1);

		cart.applyDiscounts(campaign1, campaign2, campaign3);
		cart.applyCoupon(coupon);

		cart.print();
	}
}
