package com.trendyolkb.shoppingcart.service.cart;

import com.trendyolkb.shoppingcart.domain.Product;
import com.trendyolkb.shoppingcart.domain.discount.Campaign;
import com.trendyolkb.shoppingcart.domain.discount.Coupon;

public interface ShoppingCartService {

	boolean addItem(Product product, int quantity);

	void applyDiscounts(Campaign... campaigns);

	void applyCoupon(Coupon coupon);

	double getTotalAmountAfterDiscounts();

	double getCouponDiscount();

	double getCampaignDiscount();

	double getDeliveryCost();

	void print();
}
