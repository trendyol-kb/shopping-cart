package com.trendyolkb.shoppingcart.service.cart;

import com.trendyolkb.shoppingcart.domain.CartItem;
import com.trendyolkb.shoppingcart.domain.Category;
import com.trendyolkb.shoppingcart.domain.Product;
import com.trendyolkb.shoppingcart.domain.discount.Campaign;
import com.trendyolkb.shoppingcart.domain.discount.Coupon;
import com.trendyolkb.shoppingcart.service.delivery.DeliveryCostCalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.trendyolkb.shoppingcart.service.delivery.DeliveryCostCalculator.*;

public class ShoppingCart implements ShoppingCartService {

	private List<CartItem> items = new ArrayList<>();

	// Lookup instance grouped by category, initialized if campaigns are used
	private Map<Category, List<CartItem>> itemsByCategory;

	// Initialized if campaigns are used
	private Map<Category, CategoryGroupCartInfo> categoryCartInfoMap;

	private double couponDiscount;

	private void initializeLookUp() {
		// Group items
		itemsByCategory = items.stream().collect(Collectors.groupingBy(cartItem -> cartItem.getProduct().getCategory()));

		// Prepare info data
		categoryCartInfoMap = new HashMap<>();
		itemsByCategory.forEach((category, items) -> categoryCartInfoMap.put(category, prepareCartInfo(items)));
	}

	private CategoryGroupCartInfo prepareCartInfo(List<CartItem> cartItems) {
		Double totalPrice = cartItems.stream().map(cartItem ->
				cartItem.getProduct().getPrice() * cartItem.getQuantity()
		).reduce(0.0, Double::sum);

		Integer quantitySum = cartItems.stream().map(CartItem::getQuantity).reduce(0, Integer::sum);

		return new CategoryGroupCartInfo(totalPrice, quantitySum, 0.0);
	}

	private double getTotalAmountAfterCampaigns() {
		if (categoryCartInfoMap != null) {
			return categoryCartInfoMap.values().stream().map(CategoryGroupCartInfo::getDiscountedPrice).reduce(Double::sum).orElse(0.0);
		} else {
			return items.stream().map(CartItem::getTotalPrice).reduce(Double::sum).orElse(0.0);
		}
	}

	@Override
	public boolean addItem(Product product, int quantity) {
		return items.add(new CartItem(product, quantity));
	}

	@Override
	public void applyDiscounts(Campaign... campaigns) {
		initializeLookUp();

		Stream.of(campaigns).forEach(campaign -> {
			if (categoryCartInfoMap.containsKey(campaign.getCategory())) {
				CategoryGroupCartInfo categoryGroupCartInfo = categoryCartInfoMap.get(campaign.getCategory());

				// If campaign is applicable
				if (categoryGroupCartInfo.getTotalQuantity() >= campaign.getMinQuantity()) {
					double currentDiscount = campaign.calculateDiscountFor(categoryGroupCartInfo.getTotalPrice());
					if (currentDiscount > categoryGroupCartInfo.getCampaignDiscount()) {
						categoryGroupCartInfo.setCampaignDiscount(currentDiscount);
						categoryCartInfoMap.put(campaign.getCategory(), categoryGroupCartInfo);
					}
				}
			}
		});
	}

	@Override
	public void applyCoupon(Coupon coupon) {
		// If campaign is applicable
		double totalAmountAfterCampaigns = getTotalAmountAfterCampaigns();
		if (totalAmountAfterCampaigns >= coupon.getMinAmount()) {
			couponDiscount = coupon.calculateDiscountFor(totalAmountAfterCampaigns);
		}
	}

	@Override
	public double getTotalAmountAfterDiscounts() {
		return getTotalAmountAfterCampaigns() - couponDiscount;
	}

	@Override
	public double getCouponDiscount() {
		return couponDiscount;
	}

	@Override
	public double getCampaignDiscount() {
		return categoryCartInfoMap == null ? 0.0 :
				categoryCartInfoMap.values().stream()
						.map(CategoryGroupCartInfo::getCampaignDiscount)
						.reduce(Double::sum).orElse(0.0);
	}

	@Override
	public double getDeliveryCost() {
		if (itemsByCategory == null) {
			initializeLookUp();
		}

		DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(DEFAULT_COST_PER_DELIVERY, DEFAULT_COST_PER_PRODUCT, DEFAULT_FIXED_COST);
		return deliveryCostCalculator.calculateFor(this);
	}

	@Override
	public void print() {
		if (itemsByCategory == null) {
			initializeLookUp();
		}

		itemsByCategory.forEach((category, cartItems) -> {
			System.out.println("Category: " + category.getTitle());

			cartItems.stream().forEach(cartItem -> {
				System.out.printf("\tProduct: %s ", cartItem.getProduct().getTitle());
				System.out.printf("\tQuantity: %d ", cartItem.getQuantity());
				System.out.printf("\tUnit Price: %.2f ", cartItem.getProduct().getPrice());
				System.out.printf("\tTotal Price: %.2f ", cartItem.getTotalPrice());
				System.out.println();
			});

			System.out.printf("\tCampaign Discount: %.2f", categoryCartInfoMap.get(category).getCampaignDiscount());
			System.out.println();
		});
		System.out.printf("Total Campaign Discount: %.2f\n", getCampaignDiscount());
		System.out.printf("Total Coupon Discount: %.2f\n", couponDiscount);
		double totalAmount = getTotalAmountAfterDiscounts();

		System.out.printf("Total Amount: %.2f\t Delivery Cost: %.2f", totalAmount, getDeliveryCost());
		System.out.println();
	}

	public int getProductCount() {
		return items.size();
	}

	public int getCategoryCount() {
		return items.stream()
				.map(cartItem -> cartItem.getProduct().getCategory())
				.collect(Collectors.toSet())
				.size();
	}

	private class CategoryGroupCartInfo {
		private double totalPrice;
		private int totalQuantity;
		private double campaignDiscount;

		public CategoryGroupCartInfo(double totalPrice, int totalQuantity, double campaignDiscount) {
			this.totalPrice = totalPrice;
			this.totalQuantity = totalQuantity;
			this.campaignDiscount = campaignDiscount;
		}

		public double getTotalPrice() {
			return totalPrice;
		}

		public int getTotalQuantity() {
			return totalQuantity;
		}

		public double getCampaignDiscount() {
			return campaignDiscount;
		}

		public void setCampaignDiscount(double campaignDiscount) {
			this.campaignDiscount = campaignDiscount;
		}

		public double getDiscountedPrice() {
			return totalPrice - campaignDiscount;
		}
	}
}
