package com.trendyolkb.shoppingcart.service.delivery;

import com.trendyolkb.shoppingcart.service.cart.ShoppingCart;

public interface DeliveryCostCalculatorService {
	double calculateFor(ShoppingCart cart);
}
