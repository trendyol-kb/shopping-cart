package com.trendyolkb.shoppingcart.service.delivery;

import com.trendyolkb.shoppingcart.service.cart.ShoppingCart;

public class DeliveryCostCalculator implements DeliveryCostCalculatorService {

	public static final double DEFAULT_COST_PER_DELIVERY = 10.0;
	public static final double DEFAULT_COST_PER_PRODUCT = 5.0;
	public static final double DEFAULT_FIXED_COST = 2.99;


	private double costPerDelivery = DEFAULT_COST_PER_DELIVERY;

	private double costPerProduct = DEFAULT_COST_PER_PRODUCT;

	private double fixedCost = DEFAULT_FIXED_COST;

	public DeliveryCostCalculator(double costPerDelivery, double costPerProduct, double fixedCost) {
		this.costPerDelivery = costPerDelivery;
		this.costPerProduct = costPerProduct;
		this.fixedCost = fixedCost;
	}

	@Override
	public double calculateFor(ShoppingCart cart) {
		int numberOfDeliveries = cart.getCategoryCount();
		int numberOfProducts = cart.getProductCount();

		if (numberOfDeliveries == 0) {
			return 0;
		}

		return costPerDelivery * numberOfDeliveries +
				costPerProduct * numberOfProducts +
				fixedCost;
	}
}
