package com.trendyolkb.shoppingcart.domain.discount;

import com.trendyolkb.shoppingcart.domain.Category;

public class Campaign extends Discount {
	private Category category;
	private int minQuantity;

	public Campaign(Category category, double discount, int minQuantity, DiscountType discountType) {
		super(discount, discountType);
		this.category = category;
		this.minQuantity = minQuantity;
	}

	public Category getCategory() {
		return category;
	}

	public int getMinQuantity() {
		return minQuantity;
	}
}
