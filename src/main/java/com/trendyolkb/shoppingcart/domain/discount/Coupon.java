package com.trendyolkb.shoppingcart.domain.discount;

public class Coupon extends Discount {
	private double minAmount;

	public Coupon(double minAmount, double discount, DiscountType discountType) {
		super(discount, discountType);
		this.minAmount = minAmount;
	}

	public double getMinAmount() {
		return minAmount;
	}
}
