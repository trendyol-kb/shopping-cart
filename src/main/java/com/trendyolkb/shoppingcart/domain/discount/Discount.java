package com.trendyolkb.shoppingcart.domain.discount;

public class Discount {

	private double discount;
	private DiscountType discountType;

	public Discount(double discount, DiscountType discountType) {
		this.discount = discount;
		this.discountType = discountType;
	}

	public double getDiscount() {
		return discount;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public double calculateDiscountFor(Double totalPrice) {
		switch (getDiscountType()) {
			case Rate:
				return totalPrice * getDiscount() / 100.0;
			case Amount:
				return getDiscount();
		}

		return 0.0;
	}
}
