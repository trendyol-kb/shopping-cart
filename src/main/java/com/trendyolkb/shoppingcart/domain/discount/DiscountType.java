package com.trendyolkb.shoppingcart.domain.discount;

public enum DiscountType {
	Rate,
	Amount
}
