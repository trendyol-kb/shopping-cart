package com.trendyolkb.shoppingcart.domain;

import java.util.Optional;

public class Category {

	private String title; 

	private Optional<Category> parent;

	public Category(String title) {
		this.title = title;
		this.parent = Optional.empty();
	}

	public Category(String title, Optional<Category> parent) {
		this.title = title;
		this.parent = parent;
	}

	public String getTitle() {
		return title;
	}
}
