#Shopping Cart

## Build and Run

`$ ./gradlew assemble && java -jar build/libs/shopping-cart-0.0.1.jar`

## Run Tests

`$ ./gradlew clean test`